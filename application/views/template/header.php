<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registrasi | <?php echo $title ?> </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>assets/build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a class="site_title"><span>Sistem Registrasi</span></a>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="<?=base_url(); ?>"><i class="fa fa-users"></i> Data Peserta </span></a>
                  </li>
                  <li><a href="<?php echo base_url(); ?>home/tambah-peserta"><i class="fa fa-user"></i> Isi Data Peserta </span></a>
                  </li>
                  <li><a href="<?php echo base_url(); ?>home/daftar-hadir"><i class="fa fa-users"></i> Daftar Hadir </span></a>
                  </li>
                  <li><a href="<?php echo base_url(); ?>home/registrasi-peserta"><i class="fa fa-users"></i> Registrasi Peserta </span></a>
                  </li>   
                  <li><a href="<?php echo base_url(); ?>home/undian"><i class="fa fa-user"></i> Undian </span></a>
                  </li>                     
                  <li><a href="<?php echo base_url(); ?>home/data-hadiah"><i class="fa fa-archive"></i> Data Hadiah </span></a>
                  </li> 
									<li><a href="<?php echo base_url(); ?>home/data-pemenang"><i class="fa fa-users"></i> Data Pemenang </span></a>
                  </li> 
                  <!-- <li><a href="<?php echo base_url(); ?>home/qrcode"><i class="fa fa-archive"></i> QRCode </span></a>
                  </li>             -->
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

          </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
