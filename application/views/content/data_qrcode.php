<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Registrasi | Data QR Code </title>

	<!-- Bootstrap -->
	<link href="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo base_url(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- Datatables -->
	<link href="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/select2/dist/css/select2.min.css') ?>">
	<!-- validasi -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/validasi.css') ?>">

	<!-- Custom Theme Style -->
	<link href="<?php echo base_url(); ?>assets/build/css/custom.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
</head>

<body>
	<div class="container body">
		<div class="main_container">
			<!-- /top navigation -->
			<div class="right_col" role="main">
				<?php foreach ($department as $value) { ?>
					<div class="row">
						<!-- <span style="font-size: smaller;">Nama Perusahaan: Adaro Logistics - Department: <?php echo $value->department ?></span> -->
					</div>
					<div class="row">
						<?php foreach ($hasil as $result) { ?>
							<?php if ($value->department == $result->department) { ?>
								<div class="col-md-3 col-sm-3 col-xs-3" style="text-align: center;">
									<img src="<?php echo base_url() . "/qrcode/" . $result->qr_code ?>" style="width: 64px; height: 64px;"><br>
									<span style="font-size: xx-small;"><?php echo $result->kode_peserta ?></span><br>
									<span style="font-size: xx-small;"><?php echo $result->nama_peserta ?></span>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- Bootstrap -->
	<script src="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url(); ?>assets/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/iCheck/icheck.min.js"></script>
	<!-- Datatables -->
	<script src="<?php echo base_url(); ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/jszip/dist/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>
	<script src="<?php echo base_url(); ?>assets/dist/sweetalert.min.js"></script>
	<!-- Select2 -->
	<script src="<?php echo base_url('assets/dist/select2/dist/js/select2.full.min.js') ?>"></script>

	<!-- Custom Theme Scripts -->
	<script src="<?php echo base_url(); ?>assets/build/js/custom.min.js"></script>

	<!-- Initialize datetimepicker -->
</body>

</html>
<script>
	window.print();
</script>
