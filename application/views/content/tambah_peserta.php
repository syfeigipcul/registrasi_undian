        <div class="right_col" role="main">
          <div class="">            
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Isi Data Peserta</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group" id="msg">
                        <!-- <?php echo form_open_multipart('home/SimpanDataPeserta'); ?> -->
                        <!-- <?php echo $this->session->flashdata('success_msg'); ?> -->
                        <!-- <?php echo $this->session->flashdata('error_msg'); ?> -->
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Kode Peserta <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="kode_peserta" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="Kode Peserta" autocomplete="off" autofocus="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">NIK <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="nik" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="NIK" autocomplete="off">
                        </div>
                      </div>
											<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No. Urut <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="no_urut" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="No. Urut" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Karyawan <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="nama_peserta" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="Nama Karyawan" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Department</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="department" id="department" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="Department" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Perusahaan</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="nama_perusahaan" id="nama_perusahaan" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="Nama Perusahaan" autocomplete="off">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
						              <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="button" class="btn btn-success" id ="btnSimpan">Simpan</button>
                        </div>
                      </div>
                    </form>
                    <!-- <?php echo form_close(); ?> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
          // tambah data peserta
          $(document).on("click","#btnSimpan",function(){
            var data = $('#demo-form2').serialize();
            alert(data);
            // window.location = "<?php echo base_url('home/data-peserta') ?>";
            $.ajax({
              type: "POST",
              url: "<?php echo base_url('home/SimpanDataPeserta/') ?>",
              data: data,
              success:function(data){
                alert('Data Berhaasil Di tambahkan');
                swal("Berhasil", "Data Berhasil ditambahkan", "success");                
                setTimeout(function() {
                  location.reload();
                }, 5000);
                location.reload();
              },
              error:function(data){
                swal("Gagal", "Data Gagal di Tambahkan", "error");
                console.log(data);            
              }
            });
            return false;
          });
        </script>
