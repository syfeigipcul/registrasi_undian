<!DOCTYPE html>
<html lang="en">

<head>
  <style>
    .x_content {
      background-image: url('<?php echo base_url(); ?>assets/back.jpg');
      background-repeat: no-repeat;
      background-size: 100% 100%;
    }
  </style>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Registrasi | Undian Peserta </title>

  <!-- Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="<?php echo base_url(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/select2/dist/css/select2.min.css') ?>">
  <!-- validasi -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/validasi.css') ?>">

  <!-- Custom Theme Style -->
  <link href="<?php echo base_url(); ?>assets/build/css/custom.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/dist/sweetalert.css">
</head>

<body>
  <div class="container body">
    <div class="main_container">
      <!-- <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a class="site_title"><span>Sistem Registrasi</span></a>
            </div>

            <div class="clearfix"></div>

            <br />

          </div>
        </div> -->
      <!-- top navigation -->
      <!-- <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div> -->
      <!-- /top navigation -->
      <div class="right_col" role="main">
        <div class="">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Undian Peserta</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="box-header">
                </div>
                <div class="x_content">
                  <form role="form" autocomplete="off" id="form_pemenang" name="myForm" method="post">
                    <div class="form-group">
                      <label class="control-label col-md-12 col-sm-8 col-xs-6" style="text-align: center; font-size: 28px;" for="first-name">
                        <!-- <input type="text" name="jumlah" id="jumlah"> -->
                        <!-- <span id="jumlah"></span> -->
                        <?php
                        // $batasBaru = '';
                        // $batasLama = '<label id="jumlah"></label>';
                        // // $batasBaru = strval($batasLama);
                        // $doc = new DOMDocument();
                        // $doc->loadXML($batasLama);
                        // $items = $doc->getElementsByTagName('label')->item(0);
                        // if ($items->getAttribute('jumlah') == 'jumlah') {
                        //   $$batasBaru = $items->nodeValue;
                        // }
                        // echo $batasBaru;
                        // $batasBaru = $doc->saveHTML();
                        // echo intval($batasBaru);
                        // $DOM = new DOMDocument;
                        // $DOM->loadHTML($batasLama);

                        // $items = $DOM->getElementsByTagName('label');
                        // $span_list = '';

                        // for ($i = 0; $i < $items->length; $i++) {
                        //   $item = $items->item($i);

                        //   if ($item->getAttribute('jumlah') == 'jumlah') {
                        //     $span_list = $item->nodeValue;
                        //   }
                        // }

                        // echo $span_list;

                        // echo $batas;
                        ?>

                        <?php for ($i = 0; $i < $batas; $i++) { ?>
                          <input type="hidden" name="kode_peserta[]" id="kode_peserta<?php echo $i ?>">
                          <input type="hidden" name="nama_peserta[]" id="nama_peserta<?php echo $i ?>">
                          <input type="hidden" name="department[]" id="department<?php echo $i ?>">
                          <input type="hidden" name="nama_perusahaan[]" id="nama_perusahaan<?php echo $i ?>">
                        <?php } ?>

                        <!-- <input type="hidden" name="kode_peserta" id="kode_peserta">
                        <input type="hidden" name="nama_peserta" id="nama_peserta">
                        <input type="hidden" name="department" id="department"> -->
                        <img src="<?php echo base_url('assets/logo-adaro-head.png') ?>" style="width: 30%; height: 30%;">
                        <br><br>
                        <!-- <label>Doorprize</label><br> -->
                        <select class="form-control select2" id="id_hadiah" name="id_hadiah" style="width: 50%;">
                          <option value="" selected disabled>[Pilih Hadiah Doorprize]</option>
                          <?php
                          foreach ($data as $value) { ?>
                            <option value="<?php echo $value->id_hadiah ?>" <?php if($id_hadiah == $value->id_hadiah) echo "selected"; ?>><span id="nama_hadiah"><?php echo $value->nama_hadiah ?></span> - <?php echo $value->jenis_hadiah; ?> &nbsp;<span id="jumlah">(<?php echo $value->jumlah; ?>)</span></option>
                          <?php } ?>
                        </select>
                        <span class="pesan pesan-hadiah">Field Belum Di Pilih</span>
                        <br><br>
                        <?php for ($i = 0; $i < $batas; $i++) { ?>
                          <div id="dataPeserta<?php echo $i ?>">
                          </div>
                        <?php } ?>

                        <!-- <div id="dataPeserta">
                        </div><br> -->
                        <span class="pesan pesan-nama">Tidak ada nama pemenang</span>
                        <span id="tombol"><button class="btn btn-primary" type="button" id="acak" style="font-size: 24px">Acak</button></span><br><br>
                        <span id="simpan"></span>

                      </label>
                    </div>
                    <div class="form-group">
                      <table id="datatable-buttons" name="datatable-hadir" class="table table-striped table-bordered datatable-hadir" style="font-size: xx-large;">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>NIK</th>
                            <th>Nama Peserta</th>
                            <th>Department</th>
                            <th>Nama Perusahaan</th>
                            <th>Hadiah</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 1;
                          ?>
                          <?php foreach ($pemenang as $value) { ?>
                            <tr>
                              <td><?php echo $no++ ?>.</td>
                              <td><?php echo $value->nik ?></td>
                              <td><?php echo $value->nama_peserta ?></td>
                              <td><?php echo $value->department ?></td>
                              <td><?php echo $value->nama_perusahaan ?></td>
                              <td><?php echo $value->nama_hadiah ?></td>
                              <td class="center">
                                <div class="visible-md visible-lg hidden-sm hidden-xs">
                                  <a class="btn btn-danger btn-xs" id="btnHapus" data-placement="top" title="Hapus" name="<?php echo $value->nik ?>"><i class="fa fa-times fa fa-white"></i></a>
                                </div>
                              </td>
                            </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- footer content -->
      <!-- <footer>
          <div class="pull-right">
            Copyright &copy; <?php echo date('Y') ?> - <b>GipcuL Studio</b>
          </div>
          <div class="clearfix"></div>
        </footer> -->
      <!-- /footer content -->
    </div>
  </div>


  <!-- Bootstrap -->
  <script src="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo base_url(); ?>assets/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/iCheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/jszip/dist/jszip.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/sweetalert.min.js"></script>
  <!-- Select2 -->
  <script src="<?php echo base_url('assets/dist/select2/dist/js/select2.full.min.js') ?>"></script>

  <!-- Custom Theme Scripts -->
  <script src="<?php echo base_url(); ?>assets/build/js/custom.min.js"></script>

  <!-- Initialize datetimepicker -->
</body>

</html>
<script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2();
  });
  // $(document).ready(function() {
  //   $.getJSON('<?php echo base_url('home/getPemenang') ?>', function(data) {
  //     $('.datatable-hadir').dataTable({
  //       processing: true,
  //       data: data.hasil,
  //       columns: [{
  //           render: function(data, type, row, meta) {
  //             return meta.row + meta.settings._iDisplayStart + 1 + '.';
  //           }
  //         },
  //         {
  //           data: "nik"
  //         },
  //         {
  //           data: "nama_peserta"
  //         },
  //         {
  //           data: "department"
  //         },
  //         {
  //           data: "nama_pt"
  //         },
  //         {
  //           data: "nama_hadiah"
  //         },
  //         {
  //           render: function(data, type, row) {
  //             return '<td class="center">' +
  //               '<div class="visible-md visible-lg hidden-sm hidden-xs">' +
  //               '<a class="btn btn-danger btn-xs" id="btnHapus" data-placement="top" title="Hapus" name="' + row.nik + '" ><i class="fa fa-times fa fa-white"></i></a>' +
  //               '</div>' +
  //               '</td>'
  //           }
  //         }

  //       ]
  //     });
  //   });
  // });

  var interval;
  var i = 1;

  function acakFunction() {
    playInterval();
    var html = "<button class='btn btn-danger'  type='button' id='stop' style='font-size: 24px' >Stop</button>";
    document.getElementById('tombol').innerHTML = html;
    var save = "";
    document.getElementById('simpan').innerHTML = save;
  }

  function myStopFunction() {
    stopInterval();
    // generateStop();
    var html = "<button class='btn btn-primary'  type='button' id='acak' style='font-size: 24px'>Acak</button>";
    document.getElementById('tombol').innerHTML = html;
    var save = "<button class='btn btn-success'  type='button' id='btnSimpan' style='font-size: 24px'>Simpan Nama Pemenang</button>";
    document.getElementById('simpan').innerHTML = save;
  }

  function generateAcak() {
    const batas = <?php echo $batas; ?>
    // $('#dataPeserta').html(' Value : '+i++);
    $.getJSON("<?php echo base_url('home/getDataUndian') ?>", function(data) {
      for (let index = 0; index < batas; index++) {
        var value = "<b><span id='no_karyawan'>" + data.hasil[index].nik + "</span></b> - <b><span id='nama_karyawan'>" + data.hasil[index].nama_peserta + "</span></b>";
        const datas = [];
        datas[index] = value;
        document.getElementById('dataPeserta' + index).innerHTML = datas[index];
        $('#kode_peserta' + index).val(data.hasil[index].nik);
        $('#nama_peserta' + index).val(data.hasil[index].nama_peserta);
        $('#department' + index).val(data.hasil[index].department);
        $('#nama_perusahaan' + index).val(data.hasil[index].nama_perusahaan);
        // console.log(datas);
      }

      // $.getJSON("<?php echo base_url('home/getDataUndian') ?>", function(data) {
      //   var value = " <b><span id='no_karyawan'>" + data.hasil[0].nik + "</span></b> - <b><span id='nama_karyawan'>" + data.hasil[0].nama_peserta + "</span></b>";
      //   document.getElementById('dataPeserta').innerHTML = value;
      //   $('#kode_peserta').val(data.hasil[0].nik);
      //   $('#nama_peserta').val(data.hasil[0].nama_peserta);
      //   $('#department').val(data.hasil[0].department);
      // });
    });
  }

  // function generateStop(){
  //   $.getJSON( "<?php echo base_url('home/getDataUndian') ?>", function( data ) {
  // var pemenang = " ";
  // document.getElementById('dataPeserta').innerHTML = pemenang;
  // $('#no_karyawan').text(data[0].nik);
  // $('#nama_karyawan').text(data[0].nama_peserta);
  // });
  // }

  function playInterval() {
    interval = setInterval(function() {
      generateAcak();
    }, 50);
    // return false;
  }

  function stopInterval() {
    clearInterval(interval);
    // return false;
  }

  $(document)
    .on({
      click: acakFunction
    }, "#acak")
    .on({
      click: myStopFunction
    }, "#stop");

  $('#id_hadiah').change(function() {
    let id_hadiah = $(this).val();
    location.href = "<?php echo base_url('home/getUndian/') ?>"+id_hadiah;
    // $.ajax({
    //   type: "POST",
    //   url: "<?php echo base_url('home/getUndian/') ?>",
    //   data: {id_hadiah: id_hadiah},
    //   success: function(data) {
    //     alert(id_hadiah);
    //     window.location = "<?php echo base_url('home/getUndian/') ?>";
    //   },
    //   error: function(data) {
    //     swal("Gagal", data);
    //     console.log(data);
    //   }
    // });
    // return false;
    // let batas = <?php echo $batas ?>;
    // $.ajax({
    //   type: "GET",
    //   url: "<?php echo base_url('home/getBatasJumlah/') ?>" + id_hadiah,
    //   // dataType: 'json',
    //   success: function(data) {
    //     var value = (parseInt(data.jumlah) + parseInt(batas));
    //     // $('#jumlah').val(value);
    //     document.getElementById('jumlah').innerHTML = value;
    //   },
    //   error: function(data) {
    //     swal("Gagal", "Nama Peserta Sudah Ada", "error");
    //     console.log(data);
    //   }
    // });
  });

  // tambah data peserta
  $(document).on("click", "#btnSimpan", function() {
    // var nik = $('#kode_peserta').val();
    // var nama_peserta = $('#nama_peserta').val();
    // var department = $('#department').val();
    // var id_hadiah = $('#id_hadiah').val();
    var data = $('#form_pemenang').serialize();
    var jml_hadiah = $('#id_hadiah').val().length;
    if (jml_hadiah == 0) {
      $(".pesan-hadiah").css('display', 'block');
      return false;
    }
    if (data == 0) {
      $(".pesan-nama").css('display', 'block');
      return false;
    }
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('home/simpanPemenang/') ?>",
      data: data,
      success: function(data) {
        location.reload();
      },
      error: function(data) {
        swal("Gagal", "Nama Peserta Sudah Ada", "error");
        console.log(data);
      }
    });
    return false;
  });

  //hapus data peserta
  $(document).on("click", "#btnHapus", function() {
    var nik = $(this).attr("name");
    // alert(kode_peserta);
    swal({
        title: "Hapus Data Pemenang",
        text: "Yakin akan menghapus semua peserta ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Hapus",
        closeOnConfirm: true,
      },
      function() {
        $.ajax({
          url: "<?php echo base_url() ?>home/hapus_data_pemenang/" + nik,
          success: function(data) {
            // location.reload();
            swal("Berhasil", "Data Berhasil di Hapus", "success");
            setTimeout(function() {
              location.reload();
            }, 1000);
          },
          error: function(data) {
            swal("Gagal", "Data Gagal di Hapus", "error");
            console.log(data);
          }
        });
      });
  });
</script>
