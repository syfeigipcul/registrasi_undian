		<div class="right_col" role="main">
			<div class="">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Registrasi Peserta</h2>
								<div class="clearfix"></div>
							</div>
							<form role="form" autocomplete="off" id="form_hadir" name="myForm" method="post">
								<div class="form-group">
									<input id="kode_peserta" name="kode_peserta" type="text" placeholder="Kode Peserta" autocomplete="off" autofocus="" style="height: 30px">
								</div>
							</form>
							<div class="box-header">
							</div>
							<div class="x_content">
								<!-- <div class="form-group">
                        <label for="last-name" style="font-size: 20px;">NIK :</label>
                      </div>
                      <div class="form-group">
                        <label for="last-name" style="font-size: 20px;">Nama Karyawan :</label>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" style="font-size: 20px;">Department :</label>
                      </div> -->
								<div class="form-group">
									<table table id="datatable-buttons" name="datatable-hadir" class="table table-striped table-bordered datatable-hadir">
										<thead>
											<tr>
												<th>No.</th>
												<th>NIK</th>
												<th>Nama Peserta</th>
												<th>Department</th>
												<th>Nama Perusahaan</th>
												<th>Status</th>
												<th>Waktu Registrasi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											?>
											<?php foreach ($hasil as $value) { ?>
												<tr>
													<td><?php echo $no++ ?>.</td>
													<td><?php echo $value->nik ?></td>
													<td><?php echo $value->nama_peserta ?></td>
													<td><?php echo $value->department ?></td>
													<td><?php echo $value->nama_perusahaan ?></td>
													<td><?php if ($value->status == 0) {
															echo "Tidak Hadir";
														} else {
															echo "Hadir";
														} ?></td>
													<td><?php if ($value->status == 0) {
															echo "-";
														} else {
															echo $value->waktu_registrasi;
														}
														?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			// fungsi tambah daftar hadir
			$(document).ready(function() {
				$('#kode_peserta').keyup(function(e) {
					var kode_peserta = $(this).val();
					var data = $('#form_hadir').serialize();
					var hasil = "";
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('home/SimpanDaftarHadir') ?>",
						data: data,
						success: function(data) {
							// swal("Berhasil", "Selamat Datang, Silahkan Masuk", "success");                
							//   setTimeout(function() {
							//   location.reload();
							// }, 1000);
							// location.reload();
							window.location.href = "<?php echo base_url('home/data-registrasi/') ?>"+kode_peserta;
							console.log(data);
						},
						error: function(data) {
							console.log(data);
						}
					});
				});
			});

			// $(document).ready(function() {
			//   $.getJSON('<?php echo base_url('home/getDaftarHadir') ?>', function(data) {
			//     $('.datatable-hadir').dataTable({
			//       processing: true,
			//       data: data.hasil,
			//       columns: [{
			//           render: function(data, type, row, meta) {
			//             return meta.row + meta.settings._iDisplayStart + 1 + '.';
			//           }
			//         },
			//         {
			//           data: "nik"
			//         },
			//         {
			//           data: "nama_peserta"
			//         },
			//         {
			//           data: "department"
			//         },
			//         {
			//           data: "nama_pt"
			//         },
			//         {
			//           render: function(data, type, row, meta) {
			//             if (row.status == 0) {
			//               return 'Belum Hadir';
			//             } else {
			//               return 'Hadir'
			//             };
			//           }
			//         },
			//         {
			//           render: function(data, type, row, meta) {
			//             if (row.status == 0) {
			//               return '-';
			//             } else {
			//               return row.waktu_registrasi;
			//             };
			//           }
			//         }
			//       ]
			//     });
			//   });
			// });
		</script>
