		<div class="right_col" role="main">
			<div class="">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>Daftar Hadir</h2>
							<div class="clearfix"></div>
						</div>
						<form role="form" autocomplete="off" id="form_hadir" name="myForm" method="post">
							<div class="form-group">
								<!-- <input type="text" hidden="" value="<?php echo $waktu ?>" name="waktu_registrasi"></input> -->
								<!-- <input id="qrcode" name="qrcode" required="" type="text" placeholder="Kode Peserta" autocomplete="off" autofocus=""> -->
							</div>
						</form>
						<div class="box-header">
							<button id="hapusHadir" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">Hapus Semua Data <i class="fa fa-times fa fa-white"></i></button>
						</div>
						<div class="x_content">
							<table table id="datatable-buttons" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>No.</th>
										<th>NIK</th>
										<th>Nama Peserta</th>
										<th>Department</th>
										<th>Nama Perusahaan</th>
										<th>Waktu Registrasi</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									?>
									<?php foreach ($hasil as $value) { ?>
										<tr>
											<td><?php echo $no++ ?>.</td>
											<td><?php echo $value->nik ?></td>
											<td><?php echo $value->nama_peserta ?></td>
											<td><?php echo $value->department ?></td>
											<td><?php echo $value->nama_perusahaan ?></td>
											<td><?php echo $value->waktu_registrasi ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			// tampil daftar hadir
			// $(document).ready(function() {
			//   $.getJSON('<?php echo base_url('home/getDaftarHadir') ?>', function(data) {
			//     $('#datatable-hadir').dataTable({
			//       processing: true,
			//       data: data.hasil,
			//       columns: [{
			//           render: function(data, type, row, meta) {
			//             return meta.row + meta.settings._iDisplayStart + 1 + '.';
			//           }
			//         },
			//         {
			//           data: "nik"
			//         },
			//         {
			//           data: "nama_peserta"
			//         },
			//         {
			//           data: "department"
			//         },
			//         {
			//           data: "nama_pt"
			//         },
			//         {
			//           render: function(data, type, row, meta) {
			//             if (row.status == 0) {
			//               return 'Belum Hadir';
			//             } else {
			//               return 'Hadir'
			//             };
			//           }
			//         },
			//         {
			//           render: function(data, type, row, meta) {
			//             if (row.status == 0) {
			//               return '-';
			//             } else {
			//               return row.waktu_registrasi;
			//             };
			//           }
			//         }
			//       ]
			//     });
			//   });
			// });



			//hapus semua peserta hadir
			$(document).on("click", "#hapusHadir", function() {
				swal({
						title: "Hapus Daftar Hadir",
						text: "Yakin akan menghapus semua peserta ini?",
						type: "warning",
						showCancelButton: true,
						confirmButtonText: "Hapus",
						closeOnConfirm: true,
					},
					function() {
						$.ajax({
							url: "<?php echo base_url() ?>home/hapusSemuaDataHadir/",
							success: function(data) {
								// location.reload();
								swal("Berhasil", "Data Berhasil di Hapus", "success");
								setTimeout(function() {
									location.reload();
								}, 1000);
							},
							error: function(data) {
								swal("Gagal", "Data Gagal di Hapus", "error");
								console.log(data);
							}
						});
					});
			});
		</script>
