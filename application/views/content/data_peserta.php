			<div class="right_col" role="main">
			  <div class="">
			    <div class="col-md-12 col-sm-12 col-xs-12">
			      <div class="x_panel">
			        <div class="x_title">
			          <h2>Data Peserta</h2>
			          <div class="clearfix"></div>
			        </div>
			        <div class="box-header">
			          <button id="btndeleteall" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">Hapus Semua Data Peserta <i class="fa fa-times fa fa-white"></i></button>
					  <a href="<?php echo base_url(); ?>home/qrcode" class="btn btn-info btn-xs" data-placement="top" title="Cetak" target="__blank">Cetak QR Code <i class="fa fa-print fa fa-white"></i></a>
			        </div>
			        <div class="x_content">
			          <table table id="datatable-buttons" class="table table-striped table-bordered">
			            <thead>
			              <tr>
			                <th>No.</th>
			                <th>NIK</th>
			                <th>Nama Karyawan</th>
			                <th>Department</th>
							<th>Nama Perusahaan</th>
			                <th class="hidden-xs center">Aksi Data</th>
			              </tr>
			            </thead>
			            <tbody>
			              <?php
                    $no = 1;
                    ?>
			              <?php foreach ($hasil as $value) { ?>
			                <tr>
			                  <td><?php echo $no++ ?>.</td>
			                  <td><?php echo $value->nik ?></td>
			                  <td><?php echo $value->nama_peserta ?></td>
			                  <td><?php echo $value->department ?></td>
							  <td><?php echo $value->nama_perusahaan ?></td>

			                  <td class="center">
			                    <div class="visible-md visible-lg hidden-sm hidden-xs">
			                      <a data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-default btn-xs" href="<?php echo base_url() ?>home/edit-data-peserta/<?php echo $value->kode_peserta ?>"><i class="fa fa-edit"></i></a>
			                      <a class="btn btn-danger btn-xs" id="btnHapusPeserta" data-toggle="tooltip" data-placement="top" title="Delete" name="<?php echo $value->kode_peserta ?>"><i class="fa fa-times fa fa-white"></i></a>
			                    </div>
			                  </td>
			                </tr>
			              <?php } ?>
			            </tbody>
			          </table>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			<script type="text/javascript">
			  //hapus data peserta
			  $(document).on("click", "#btnHapusPeserta", function() {
			    var kode_peserta = $(this).attr("name");
			    // alert(kode_peserta);
			    swal({
			        title: "Hapus Semua Peserta",
			        text: "Yakin akan menghapus semua peserta ini?",
			        type: "warning",
			        showCancelButton: true,
			        confirmButtonText: "Hapus",
			        closeOnConfirm: true,
			      },
			      function() {
			        $.ajax({
			          url: "<?php echo base_url() ?>home/hapus_data_peserta/" + kode_peserta,
			          success: function(data) {
			            // location.reload();
			            swal("Berhasil", "Data Berhasil di Hapus", "success");
			            setTimeout(function() {
			              location.reload();
			            }, 1000);
			          },
			          error: function(data) {
			            swal("Gagal", "Data Gagal di Hapus", "error");
			            console.log(data);
			          }
			        });
			      });
			  });

			  //hapus semua data peserta
			  $(document).on("click", "#btndeleteall", function() {
			    // alert('masuk');
			    swal({
			        title: "Hapus Semua Peserta",
			        text: "Yakin akan menghapus semua peserta ini?",
			        type: "warning",
			        showCancelButton: true,
			        confirmButtonText: "Hapus",
			        closeOnConfirm: true,
			      },
			      function() {
			        $.ajax({
			          url: "<?php echo base_url() ?>home/hapusSemuaDataPeserta/",
			          success: function(data) {
			            // location.reload();
			            swal("Berhasil", "Data Berhasil di Hapus", "success");
			            setTimeout(function() {
			              location.reload();
			            }, 1000);
			          },
			          error: function(data) {
			            swal("Gagal", "Data Gagal di Hapus", "error");
			            console.log(data);
			          }
			        });
			      });
			  });
			</script>
