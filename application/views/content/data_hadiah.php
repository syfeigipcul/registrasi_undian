        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Isi Data Hadiah</h2>

                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group" id="msg">
                        <!-- <?php echo form_open_multipart('home/SimpanDataPeserta'); ?> -->
                        <!-- <?php echo $this->session->flashdata('success_msg'); ?> -->
                        <!-- <?php echo $this->session->flashdata('error_msg'); ?> -->
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Hadiah <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="nama_hadiah" id="nama_hadiah" type="text" class="form-control col-md-7 col-xs-12" placeholder="Nama Hadiah" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jenis Hadiah <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="jenis_hadiah" id="jenis_hadiah" type="text" class="form-control col-md-7 col-xs-12" placeholder="Jenis Hadiah" autocomplete="off">
                        </div>
                      </div>
											<!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kategori Hadiah <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
												<select class="form-control select2" id="kategori" name="kategori" style="width: 50%;">
													<option value="" disabled>[Pilih Kategori]</option>
														<option value="Gold">Gold</option>
														<option value="Silver">Silver</option>
														<option value="Bronze">Bronze</option>
												</select>
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jumlah <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="jumlah" id="jumlah" type="number" class="form-control col-md-7 col-xs-12" placeholder="Jumlah" autocomplete="off">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="button" class="btn btn-success" id="btnSimpan">Simpan</button>
                        </div>
                      </div>
                    </form>
                    <div align="center">
                      <table table id="datatable-hadiah" class="table table-striped table-bordered" style="width: 50%;">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>Nama Hadiah</th>
                            <th>Jenis Hadiah</th>
														<!-- <th>Kategori</th> -->
                            <th>Jumlah</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <!-- <?php echo form_close(); ?> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Button trigger modal -->


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nama Hadiah</label>
                      <div class="col-sm-6">
                        <input type="hidden" class="form-control" name="id_hadiah_edit" id="id_hadiah_edit" autocomplete="off">
                        <input type="text" class="form-control" name="nama_hadiah_edit" id="nama_hadiah_edit" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Jenis Hadiah</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="jenis_hadiah_edit" id="jenis_hadiah_edit" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div>
								<!-- <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Kategori</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="kategori_edit" id="kategori_edit" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div> -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Jumlah</label>
                      <div class="col-sm-6">
                        <input type="number" class="form-control" name="jumlah_edit" id="jumlah_edit" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="btnUpdate" class="btn btn-primary">Update</button>
              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          $(document).ready(function() {
            $.getJSON('<?php echo base_url('home/getDataHadiah') ?>', function(data) {
              $('#datatable-hadiah').dataTable({
                processing: true,
                data: data.hasil,
                columns: [{
                    render: function(data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1 + '.';
                    }
                  },
                  {
                    data: "nama_hadiah"
                  },
                  {
                    data: "jenis_hadiah"
                  },
                  {
                    data: "jumlah"
                  },
                  {
                    render: function(data, type, row) {
                      return '<td class="center">' +
                        '<div class="visible-md visible-lg hidden-sm hidden-xs">' +
                        ' <a class="btn btn-default btn-xs" id="btnEdit" data-placement="top" title="Edit" name="btnEdit" data-id="' + row.id_hadiah + '" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-edit"></i></a>' +
                        ' <a class="btn btn-danger btn-xs" id="btnHapus" data-placement="top" title="Hapus" name="' + row.id_hadiah + '" ><i class="fa fa-times fa fa-white"></i></a>' +
                        '</div>' +
                        '</td>'
                    }
                  }
                ]
              });
            });
          });

          // tambah data hadiah
          $(document).on("click", "#btnSimpan", function() {
            var data = $('#demo-form2').serialize();
            $.ajax({
              type: "POST",
              url: "<?php echo base_url('home/insertHadiah/') ?>",
              data: data,
              success: function(data) {
                swal("Berhasil", "Data Berhasil ditambahkan", "success");
                setTimeout(function() {
                  location.reload();
                }, 1000);
              },
              error: function(data) {
                swal("Gagal", "Data Gagal di Tambahkan", "error");
                console.log(data);
              }
            });
            return false;
          });

          // update data hadiah
          $(document).on("click", "#btnUpdate", function() {
            var id_hadiah = $('#id_hadiah_edit').val();
            var nama_hadiah = $('#nama_hadiah_edit').val();
						// var kategori = $('#kategori_edit').val();
            var jenis_hadiah = $('#jenis_hadiah_edit').val();

            var jumlah = $('#jumlah_edit').val();
            $.ajax({
              type: "POST",
              url: "<?php echo base_url('home/updateHadiah/') ?>",
              data: {
                id_hadiah: id_hadiah,
                nama_hadiah: nama_hadiah,
                jenis_hadiah: jenis_hadiah,
								// kategori: kategori,
                jumlah: jumlah
              },
              success: function(data) {
                swal("Berhasil", "Data Berhasil ditambahkan", "success");
                setTimeout(function() {
                  location.reload();
                }, 1000);
              },
              error: function(data) {
                swal("Gagal", "Data Gagal di Tambahkan", "error");
                console.log(data);
              }
            });
            return false;
          });

          //hapus data peserta
          $(document).on("click", "#btnHapus", function() {
            var id_hadiah = $(this).attr("name");
            // alert(kode_peserta);
            swal({
                title: "Hapus Semua Peserta",
                text: "Yakin akan menghapus semua peserta ini?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Hapus",
                closeOnConfirm: true,
              },
              function() {
                $.ajax({
                  url: "<?php echo base_url() ?>home/deleteHadiah/" + id_hadiah,
                  success: function(data) {
                    // location.reload();
                    swal("Berhasil", "Data Berhasil di Hapus", "success");
                    setTimeout(function() {
                      location.reload();
                    }, 1000);
                  },
                  error: function(data) {
                    swal("Gagal", "Data Gagal di Hapus", "error");
                    console.log(data);
                  }
                });
              });
          });

          $(document).on("click", "#btnEdit", function() {
            var id_hadiah = $(this).data('id');
            $.ajax({
              url: "<?php echo base_url('home/getHadiah') ?>",
              method: "GET",
              data: {
                id_hadiah: id_hadiah
              },
              dataType: 'json',
              cache: false,
              success: function(data) {
                $('[name="id_hadiah_edit"]').val(data.id_hadiah);
                $('[name="nama_hadiah_edit"]').val(data.nama_hadiah);
                $('[name="jenis_hadiah_edit"]').val(data.jenis_hadiah);
								// $('[name="kategori_edit"]').val(data.kategori);
                $('[name="jumlah_edit"]').val(data.jumlah);
              },
              error: function(data) {
                console.log(data);
              }
            });
          });
        </script>
