        <div class="right_col" role="main">
          <div class="">
            
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Data Peserta</h2>
                    
                    <div class="clearfix"></div>
                  </div>

                  <!-- <?php echo form_open_multipart('home/UpdateDataPeserta/'.$kode_peserta); ?> -->
                  <div class="x_content">
                    <br />

                    
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                      
                      <div class="form-group" id="msg">
                        
                      </div>                          
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No. Karyawan <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="kode_peserta" required="" type="hidden" class="form-control col-md-7 col-xs-12" placeholder="Kode Peserta" id="kode_peserta" autocomplete="off" hidden="" value="<?php echo $kode_peserta ?>" readonly="true">
                          <input name="nik" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="NIK" autocomplete="off" value="<?php echo $nik ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Peserta <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="nama_peserta" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="Nama Peserta" autocomplete="off" value="<?php echo $nama_peserta ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Department</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="department" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="Department" autocomplete="off" value="<?php echo $department ?>">
                        </div>
                      </div>
											<div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Perusahaan</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="nama_perusahaan" required="" type="text" class="form-control col-md-7 col-xs-12" placeholder="Department" autocomplete="off" value="<?php echo $nama_perusahaan ?>">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button" onclick="history.back(-1)" >Kembali</button>
                          <button type="button" class="btn btn-success" name="btnupdate" id="btnupdate">Submit <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                      </div>
                    </form>
                     <!-- <?php echo form_close(); ?> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
          // update data peserta
          $(document).on("click","#btnupdate",function(){
            var kode_peserta = $('#kode_peserta').val();
            var data = $('#demo-form2').serialize();
            // alert(data);
            // window.location = "<?php echo base_url('home/data-peserta') ?>";
            $.ajax({
              type: "POST",
              url: "<?php echo base_url('home/UpdateDataPeserta/') ?>"+kode_peserta,
              data: data,
              success:function(data){
                // console.log(data);
                swal("Berhasil", "Data Berhasil di Hapus", "success");                
                  setTimeout(function() {
                  location.reload();
                }, 1000);
                
              },
              error:function(data){
                swal("Gagal", "Data Gagal di Perbarui", "error");
                console.log(data);            
              }
            });
          });
        </script>

        