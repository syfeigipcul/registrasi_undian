<!-- <Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Peserta
        <small>Tambah Data Peserta</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header with-border">
              <i class="fa fa-users"></i>
              <h3 class="box-title">Data Peserta</h3>             
            </div>
            <?php echo form_open_multipart('home/SimpanDataPeserta'); ?> 
            
            <div class="box-body">
              <form role="form" autocomplete="off" id="form-user" method="post">
                <div class="form-group" id="msg">
                  <?php echo $this->session->flashdata('success_msg'); ?>
                  <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
              	<div class="form-group">
                  <label>Kode Peserta</label>
                  <input name="kode_peserta" required="" type="text" class="form-control" placeholder="Kode Peserta" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>Nama Peserta</label>
                  <input name="nama_peserta" required="" type="text" class="form-control" placeholder="Nama Peserta" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input name="alamat" required="" type="text" class="form-control" placeholder="Alamat" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>No. HP</label>
                  <input name="no_hp" type="text" class="form-control" placeholder="No. HP" autocomplete="off">
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button class="btn btn-primary" type="reset">Reset</button>
                    <button type="submit" id ="btn_simpan" class="btn btn-success">Submit <i class="fa fa-arrow-circle-right"></i></button>
                  </div>
                </div>        
              </form>
            </div>            
            <?php echo form_close(); ?>
          </div>
        </section>
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper
  -->
  