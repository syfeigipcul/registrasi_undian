<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Peserta
        <small>Data Peserta</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Semua Data Peserta</h3>
              
            </div>
            <div class="box-header">
              <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete" id="btn_allDelete">Hapus Semua Data Peserta  <i class="fa fa-times fa fa-white"></i></button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-stripped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode Peserta</th>
                  <th>Nama Peserta</th>
                  <th>Alamat</th>
                  <th>No. HP</th>
                  <th>QR Code</th>
                  <th class="hidden-xs center">Aksi Data</th>
                </tr>
                </thead>
                <tbody>
                  <?php  
                    $no = 1;
                  ?>
                <?php foreach($hasil as $value) { ?>
                <tr>
                  <td><?php echo $no++?>.</td>
                  <td><?php echo $value->kode_peserta ?></td>
                  <td><?php echo $value->nama_peserta ?></td>
                  <td><?php echo $value->alamat ?></td>
                  <td><?php echo $value->no_hp ?></td>
                  <td><img style="width: 50px;" src="<?php echo base_url().'qrcode/'.$value->qr_code;?>"></td>
                  
                  <td class="center">
                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                      <a data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-default btn-xs" href="<?php echo base_url() ?>home/edit-data-peserta/<?php echo $value->kode_peserta ?>"><i class="fa fa-edit"></i></a>
                      <a onclick="return confirm('Apakah anda yakin akan menghapus data ini ?');" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete" href="<?php echo base_url() ?>home/hapus_data_peserta/<?php echo $value->kode_peserta ?>"><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                  </td>
                </tr>      
                <?php } ?>         
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
