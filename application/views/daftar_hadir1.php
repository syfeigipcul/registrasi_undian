<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Peserta
        <small>Data Peserta</small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Semua Data Peserta</h3>              
            </div>
            <!-- <?php echo form_open_multipart('home/SimpanDaftarHadir'); ?>  -->
            <!-- <?php echo $this->session->flashdata('success_msg'); ?>
            <?php echo $this->session->flashdata('error_msg'); ?> -->
            <div class="box-header">
              <button id="btn_hapusHadir" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">Hapus Semua Data  <i class="fa fa-times fa fa-white"></i></button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" autocomplete="off" id="myForm" name="myForm" method="post" >
                <div class="form-group">
                  <?php
                  date_default_timezone_set('Asia/Shanghai');
                  $waktu = date("h:i:sa"); ?>
                  <input type="text" hidden="" value="<?php echo $waktu ?>" name="waktu_registrasi"></input>
                  <input id="kode-peserta" name="kode_peserta" required="" type="text" placeholder="Kode Peserta" autocomplete="off" autofocus="" onkeydown="if(event.keyCode == 13){document.getElementById('myForm').submit();}">
                </div>
              </form>
              <!-- <?php echo form_close();?> -->
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-stripped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Peserta</th>
                  <th>Waktu Registrasi</th>
                </tr>
                </thead>
                <tbody>
                  <?php  
                    $no = 1;
                  ?>
                <?php foreach($hasil as $value) { ?>
                <tr>
                  <td><?php echo $no++?>.</td>
                  <td><?php echo $value->nama_peserta ?></td>
                  <td><?php echo $value->waktu_registrasi ?></td>            
                </tr>      
                <?php } ?>         
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>

  
