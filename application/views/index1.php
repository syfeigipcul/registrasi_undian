<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title ?></title>

 
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/dist/sweetalert.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Sistem Registrasi</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?=base_url(); ?>">
            <i class="fa fa-user"></i> <span>Data Peserta</span>            
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>home/tambah-peserta">
            <i class="fa fa-user"></i> <span>Isi Data Peserta</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url(); ?>home/daftar-hadir">
            <i class="fa fa-user"></i> <span>Daftar Hadir Peserta</span>
          </a>
        </li>      
        <li>
          <a href="<?php echo base_url(); ?>home/undian">
            <i class="fa fa-user"></i> <span>Undian</span>
          </a>
        </li>  
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <?php $this->load->view($page); ?>

  <footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y') ?> <a href="https://adminlte.io">GipcuL Studio</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/admin/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(); ?>assets/admin/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/admin/dist/js/demo.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/dist/sweetalert.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>

<script>
  $(function () {

    $('#example1').DataTable()

    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })

  })
</script>
<script type="text/javascript">
  //fungsi tambah peserta
    // $(document).ready(function(){
    //   $('#btn_simpan').click(function(){
    //     var data = $('#form-user').serialize();
    //     $.ajax({
    //       type: "POST",
    //       url: "<?php echo base_url('home/SimpanDataPeserta'); ?>",
    //       data: data,
    //       success:function(data){
    //         // swal({
    //         //       type: 'success',
    //         //       title: 'Tambah Mahasiswa',
    //         //       text: 'Anda Berhasil Menambah Mahasiswa'
    //         // });
    //       },
    //       error:function(data){
    //         // swal("Oops...", "Terjadi Kesalahan :(", "error");
    //       }
    //     });

    //   });
    // });

    // fungsi tambah daftar hadir
    $(document).ready(function(){
      $('#kode_peserta').keyup(function(e){
        var data = $('#myForm').serialize();
        // alert('masuk');
        $.ajax({
          type: "POST",
          url: "<?php echo base_url('home/SimpanDaftarHadir') ?>",
          data: data,
          success:function(data){
            location.reload();
          },
          error:function(data){
            console.log(data);
          }
        });
      });
    });

    //hapus data peserta
    $(document).on("click","#btn_hapus",function(){
      var kode_peserta = $('#kode_peserta').val();
     //  alert(kode_peserta);
     //  swal({
     //    title:"Hapus Peserta",
     //    text:"Yakin akan menghapus peserta ini?",
     //    type: "warning",
     //    showCancelButton: true,
     //    confirmButtonText: "Hapus",
     //    closeOnConfirm: true,
     //  },
     //  function(){
     //    $.ajax({
     //      url:"<?php echo base_url('home/hapus_data_peserta'); ?>/"+kode_peserta,
     //      data:{kode_peserta:kode_peserta},
     //      success: function(data){
     //        location.reload();
     //      },
     //      error:function(data){
     //        console.log(data);
     //        swal({
     //    title:"Hapus Gagal",
     //    text:"Terjadi Kesalahan",
     //    type: "error"
     //  });
     //      }
     //    });
     //  });
     // });

     function deletePeserta(kode_peserta){
      if (confirm("Are you sure?")) {
        $.ajax({
          url:"<?php echo base_url('home/hapus_data_peserta'); ?>/"+kode_peserta,
          data:{kode_peserta:kode_peserta},
          success: function(data){
            location.reload();
          },
          error:function(data){
            console.log(data);
            
          }
        });
        }
          else {
        }
     }

    //hapus semua data peserta
     $(document).on("click","#btn_allDelete",function(){
      swal({
        title:"Hapus Semua Peserta",
        text:"Yakin akan menghapus semua peserta ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Hapus",
        closeOnConfirm: true,
      },
      function(){
        $.ajax({
          url:"<?php echo base_url() ?>home/hapusSemuaDataPeserta/",
          success: function(data){
            location.reload();
          },
          error:function(data){
            console.log(data);
          }
        });
      });
     });

     //hapus semua peserta hadir
     $(document).on("click","#btn_hapusHadir",function(){
      swal({
        title:"Hapus Daftar Hadir",
        text:"Yakin akan menghapus semua peserta ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Hapus",
        closeOnConfirm: true,
      },
      function(){
        $.ajax({
          url: "<?php echo base_url() ?>home/hapusSemuaDataHadir/",
          success: function(data){
            location.reload();
          },
          error:function(data){
            console.log(data);
          }
        });
      });
     });    
</script>
</body>
</html>
