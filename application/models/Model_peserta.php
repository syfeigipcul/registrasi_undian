<?php

/**
 * 
 */
class Model_peserta extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
	}

	public function peserta()
	{
		// $this->db->order_by('nama_perusahaan', 'asc');
		// $this->db->order_by('department', 'asc');
		// $this->db->order_by('nama_peserta', 'asc');
		$this->db->order_by('no_urut', 'asc');
		$query = $this->db->get('peserta');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function dataPemenang()
	{
		$this->db->order_by('timestamp', 'desc');
		$query = $this->db->get('data_pemenang');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return array();
		}
	}


	public function dataPeserta()
	{
		// $this->db->order_by('nik', 'asc');
		// $this->db->order_by('nama_perusahaan', 'asc');
		// $this->db->order_by('department', 'asc');
		// $this->db->order_by('nama_peserta', 'asc');
		// $this->db->order_by('kode_peserta', 'asc');
		// $this->db->where_not_in('nik', '0');
		$this->db->order_by('no_urut', 'asc');
		$query = $this->db->get('data_peserta');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return array();
		}
	}

	function simpan_peserta($kode_peserta, $nik, $no_urut, $nama_peserta, $department, $nama_perusahaan)
	{
		$data = array(
			'kode_peserta'  => $kode_peserta,
			'nik'   => $nik,
			'no_urut' => $no_urut,
			'nama_peserta'   => $nama_peserta,
			'department'   => $department,
			'nama_perusahaan' => $nama_perusahaan
		);
		$insertData = $this->db->insert('data_peserta', $data);
		//   if($insertData){
		//     $this->session->set_flashdata('success_msg', 'Data telah berhasil di tambahkan.');  
		//   } else {              
		//     $this->session->set_flashdata('error_msg', 'Terjadi kesalahan. Silahkan coba lagi.');    
		//   }
	}

	public function simpan_qrcode($kode_peserta, $qr_code)
	{
		$data = array(
			'kode_peserta'  => $kode_peserta,
			'qr_code'       => $qr_code
		);
		$insertData = $this->db->insert('data_qrcode', $data);
	}

	public function update_peserta($kode_peserta, $nik, $nama_peserta, $department, $nama_perusahaan)
	{
		$data = array(
			'nik'   => $nik,
			'nama_peserta'  => $nama_peserta,
			'department'   => $department,
			'nama_perusahaan' => $nama_perusahaan
		);
		$this->db->where('kode_peserta', $kode_peserta);
		$this->db->update('data_peserta', $data);
	}

	public function updateKategoriPeserta($kode_peserta, $department, $nama_perusahaan)
	{
		$data = array(
			'department'   => $department,
			'nama_perusahaan' => $nama_perusahaan
		);
		$this->db->where('kode_peserta', $kode_peserta);
		$this->db->update('data_peserta', $data);
	}

	public function hapus_semua_data_peserta()
	{
		$this->db->query('DELETE FROM data_peserta');
		$this->db->query('DELETE FROM data_qrcode');
	}

	public function datahadir()
	{
		$this->db->select('*');
		$this->db->from('data_peserta');
		$this->db->order_by('waktu_registrasi', 'desc');
		// $this->db->order_by('nama_perusahaan', 'asc');
		// $this->db->order_by('department', 'asc');
		// $this->db->order_by('nama_peserta', 'asc');
		$this->db->order_by('no_urut', 'asc');
		// $this->db->from('daftar_hadir');
		// $this->db->join('data_peserta', 'data_peserta.kode_peserta = daftar_hadir.kode_peserta');
		$this->db->where('status', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function dataRegistrasi()
	{
		$this->db->select('*');
		$this->db->from('data_peserta');
		$this->db->order_by('waktu_registrasi', 'desc');
		// $this->db->order_by('nama_perusahaan', 'asc');
		// $this->db->order_by('department', 'asc');
		// $this->db->order_by('nama_peserta', 'asc');
		// $this->db->order_by('kode_peserta', 'asc');
		$this->db->order_by('no_urut', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	function simpan_daftar_hadir($kode_peserta)
	{
		// $data = array (
		// 	'kode_peserta' 		=> $kode_peserta
		// );
		// $this->db->insert('daftar_hadir', $data);
		$waktu = date("H:i:s");
		$data = array(
			'status'   => 1,
			'waktu_registrasi'  => $waktu
		);
		$this->db->where('kode_peserta', $kode_peserta);
		$this->db->update('data_peserta', $data);
	}

	function save_batch($data)
	{
		return $this->db->insert_batch('data_pemenang', $data);
	}

	function simpan_pemenang($nik, $nama_peserta, $department, $nama_perusahaan, $nama_hadiah)
	{
		$data = array(
			'nik'    => $nik,
			'nama_peserta' => $nama_peserta,
			'department' => $department,
			'nama_perusahaan' => $nama_perusahaan,
			'nama_hadiah' => $nama_hadiah
		);
		$this->db->insert('data_pemenang', $data);
		$undian = array(
			'undian'   => 1,
		);
		$this->db->where('nik', $nik);
		$this->db->update('data_peserta', $undian);
	}

	function update_pemenang($nik)
	{
		$undian = array(
			'undian'   => 1,
		);
		$this->db->where('nik', $nik);
		$this->db->update('data_peserta', $undian);
	}

	function hapus_semua_daftar_hadir()
	{
		$data = array(
			'status'   => 0,
			'waktu_registrasi'  => ""
		);
		$this->db->where('status', 1);
		$this->db->update('data_peserta', $data);
		// $this->db->query('DELETE FROM daftar_hadir');
	}

	function hapus_data_peserta($id)
	{
		return $this->db->delete('data_peserta', array('kode_peserta' => $id));
	}

	function hapus_data_pemenang($id)
	{
		return $this->db->delete('data_pemenang', array('nik' => $id));
	}

	public function get_by_id($id)
	{
		$this->db->where('kode_peserta', $id);
		return $this->db->get('data_peserta')->row();
	}

	public function checkPeserta($id)
	{
		$this->db->where('kode_peserta', $id);
		return $this->db->get('daftar_hadir')->row();
	}

	public function undian() {
		$this->db->select('*');
		$this->db->from('data_peserta');
		$this->db->order_by('kode_peserta', 'RANDOM');
		$this->db->where('status', 1);
		$this->db->where_not_in('undian', 1);
		$query = $this->db->get();
		return $query->result();	
	}

	public function undianGold($kategori)
	{
		$this->db->select('*');
		$this->db->from('data_peserta');
		// $this->db->join('daftar_hadiah', 'data_peserta.kategori = daftar_hadiah.kategori');
		$this->db->order_by('kode_peserta', 'RANDOM');
		// $this->db->where_not_in('nik', '0');
		$this->db->where('status', 1);
		$this->db->like('kategori', $kategori);
		$this->db->where_not_in('undian', 1);
		// $this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function undianSilver()
	{
		$this->db->select('*');
		$this->db->from('data_peserta');
		// $this->db->join('daftar_hadiah', 'data_peserta.kategori = daftar_hadiah.kategori');
		$this->db->order_by('kode_peserta', 'RANDOM');
		// $this->db->where_not_in('nik', '0');
		// $this->db->where('status', 1);
		$this->db->not_like('kategori', 'bronze');
		$this->db->where_not_in('undian', 1);
		// $this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function undianBronze()
	{
		$this->db->select('*');
		$this->db->from('data_peserta');
		// $this->db->join('daftar_hadiah', 'data_peserta.kategori = daftar_hadiah.kategori');
		$this->db->order_by('kode_peserta', 'RANDOM');
		// $this->db->where_not_in('nik', '0');
		// $this->db->where('status', 1);
		// $this->db->not_like('kategori', 'gold');
		// $this->db->not_like('kategori', 'silver');
		$this->db->where_not_in('undian', 1);
		// $this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
	// hadiah

	public function getAllDataHadiah()
	{
		$this->db->select('*');
		$this->db->from('daftar_hadiah');
		$this->db->order_by('nama_hadiah', 'asc');
		$query = $this->db->get();
		return $query->result();
	}
	public function getDataHadiah()
	{
		$this->db->select('*');
		$this->db->from('daftar_hadiah');
		$this->db->order_by('jenis_hadiah', 'desc');
		$this->db->where_not_in('jumlah', '0');
		$query = $this->db->get();
		return $query->result();
	}

	public function getHadiah($id_hadiah)
	{
		$this->db->where('id_hadiah', $id_hadiah);
		$query = $this->db->get('daftar_hadiah');
		return $query->row();
	}

	public function insertHadiah($nama_hadiah, $jenis_hadiah, $jumlah)
	{
		$data = array(
			'nama_hadiah'  => $nama_hadiah,
			'jenis_hadiah' => $jenis_hadiah,
			// 'kategori'		=> $kategori,
			'jumlah'       => $jumlah
		);
		$insertData = $this->db->insert('daftar_hadiah', $data);
	}

	public function updateHadiah($id_hadiah, $nama_hadiah, $jenis_hadiah, $jumlah)
	{
		$data = array(
			'nama_hadiah'  => $nama_hadiah,
			'jenis_hadiah' => $jenis_hadiah,
			// 'kategori'		=> $kategori,
			'jumlah'   => $jumlah
		);
		$this->db->where('id_hadiah', $id_hadiah);
		$this->db->update('daftar_hadiah', $data);
	}

	public function deleteHadiah($id)
	{
		return $this->db->delete('daftar_hadiah', array('id_hadiah' => $id));
	}

	public function updateJumlah($id_hadiah, $jumlah)
	{
		$data = array(
			'jumlah'   => $jumlah
		);
		$this->db->where('id_hadiah', $id_hadiah);
		$this->db->update('daftar_hadiah', $data);
	}

	public function batasJumlah($id_hadiah)
	{
		$this->db->where('id_hadiah', $id_hadiah);
		$query = $this->db->get('daftar_hadiah');
		return $query->row();
	}
	//end hadiah

	public function dataQRCode()
	{
		$this->db->select('data_qrcode.kode_peserta, data_qrcode.qr_code, data_peserta.nik, data_peserta.no_urut, data_peserta.nama_peserta, data_peserta.department, data_peserta.nama_perusahaan');
		$this->db->from('data_qrcode');
		$this->db->join('data_peserta', 'data_peserta.kode_peserta = data_qrcode.kode_peserta');
		$this->db->like('nama_perusahaan', "Undangan");
		// $this->db->where('department', "PLANT PRODUCTION DEPARTMENT");
		$this->db->order_by('no_urut', 'asc');
		// $department = array('PCH', 'PWD');
		// $this->db->where_in('department', $department);
		// $this->db->order_by('department', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function department()
	{
		// $this->db->distinct('department');
		// $this->db->from('data_peserta');
		// $this->db->like('nama_perusahaan', "PT. AI");
		$query = $this->db->query("SELECT DISTINCT department FROM data_peserta  ORDER BY department ASC;");
		return $query->result();
	}
}
