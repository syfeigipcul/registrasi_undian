<?php
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Model_peserta');
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
	}

	public function index()
	{		// untuk mengisi data otomatis
		$data = $this->Model_peserta->peserta();
		// // $i =1;
		// foreach ($data as $result) {
			// $code = $i++."-".$result->nik;
			// $kode_peserta = str_replace(" ", "", $code);
			// $kode_peserta = $result->nik."-".$result->no_urut;
			// $kategori = $result->kategori;
			// $nik = $result->nik;
			// $no_urut = $result->no_urut;
			// $nama_peserta = $result->nama_peserta;
			// $department = $result->department;
			// $nama_perusahaan = $result->nama_perusahaan;

			// $this->load->library('ciqrcode');

			// $config['cacheable']    = true; //boolean, the default is true
			// $config['cachedir']     = './assets/'; //string, the default is application/cache/
			// $config['errorlog']     = './assets/'; //string, the default is application/logs/
			// $config['imagedir']     = './qrcode/'; //direktori penyimpanan qr code
			// $config['quality']      = true; //boolean, the default is true
			// $config['size']         = '1024'; //interger, the default is 1024
			// $config['max_width']    = '132';
			// $config['max_height']   = '132';
			// $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
			// $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
			// $this->ciqrcode->initialize($config);

			// $qr_code = $kode_peserta . '.png'; //buat name dari qr code sesuai dengan nim

			// $params['data'] = $kode_peserta; //data yang akan di jadikan QR CODE
			// $params['level'] = 'H'; //H=High
			// $params['size'] = 10;
			// $params['savename'] = FCPATH . $config['imagedir'] . $qr_code; //simpan image QR CODE ke folder assets/images/
			// $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE


			// $this->Model_peserta->simpan_qrcode($kode_peserta, $qr_code); //simpan ke database

			// $this->Model_peserta->updateKategoriPeserta($kode_peserta, $department, $nama_perusahaan);
			// $this->Model_peserta->simpan_peserta($kode_peserta, $nik, $no_urut, $nama_peserta, $department, $nama_perusahaan); //simpan ke database
		// }



		$title['title'] = ('Data Peserta');
		$data = array(
			'hasil' => $this->Model_peserta->dataPeserta()
		);
		$this->load->view('template/header', $title);
		$this->load->view('content/data_peserta', $data);
		$this->load->view('template/footer');
	}

	public function daftar_hadir()
	{
		$title['title'] = ('Daftar Hadir Peserta');
		$data = array(
			'page' => 'content/daftar_hadir',
			'hasil' => $this->Model_peserta->datahadir()
		);
		$this->load->view('template/header', $title);
		$this->load->view('template/content', $data);
		$this->load->view('template/footer');
	}

	public function getDaftarHadir()
	{
		$data['hasil'] = $this->Model_peserta->datahadir();
		echo json_encode($data);
	}

	public function getPemenang()
	{
		$data['hasil'] = $this->Model_peserta->dataPemenang();
		echo json_encode($data);
	}

	public function SimpanDaftarHadir()
	{
		$kode_peserta = $this->input->post('kode_peserta');
		// $hapus_spasi = str_replace(" ", "", $kode_peserta);

		$save = $this->Model_peserta->checkPeserta($kode_peserta);
		if ($save == 0) {
			$this->Model_peserta->simpan_daftar_hadir($kode_peserta); //simpan ke database  
		} else {
			// $this->Pengunjung_m->update($ip, $tanggal, $waktu);
		}
	}

	public function simpanPemenang()
	{
		$result = array();
		$nik = $this->input->post('kode_peserta');
		$nama_peserta = $this->input->post('nama_peserta');
		$department = $this->input->post('department');
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$id_hadiah = $this->input->post('id_hadiah');
		$data = $this->Model_peserta->getHadiah($id_hadiah);

		$index = 0;
		foreach ($nik as $datanik) {
			array_push($result, array(
				'nik' => $datanik,
				'nama_peserta' => $nama_peserta[$index],
				'department'	=> $department[$index],
				'nama_perusahaan' => $nama_perusahaan[$index],
				'nama_hadiah' => $data->nama_hadiah
			));
			$index++;
			$this->Model_peserta->update_pemenang($datanik);
		}
		$sql = $this->Model_peserta->save_batch($result);
		$this->Model_peserta->updateJumlah($id_hadiah, $data->jumlah - $index);
		if ($sql) { // Jika sukses
			echo "<script>alert('Data berhasil disimpan');window.location = '" . base_url('home/undian') . "';</script>";
		} else { // Jika gagal
			echo "<script>alert('Data gagal disimpan');window.location = '" . base_url('home/undian') . "';</script>";
		}


		// $this->Model_peserta->simpan_pemenang($nik, $nama_peserta, $department, $data->nama_hadiah); //simpan ke database
		// $this->Model_peserta->updateJumlah($id_hadiah, $data->jumlah - 1);

	}

	public function hapusSemuaDataHadir()
	{
		$this->Model_peserta->hapus_semua_daftar_hadir();
		redirect('home/daftar-hadir');
	}

	public function tambah_peserta()
	{
		$title['title'] = ('Tambah Peserta');
		$data = array(
			'page' => 'content/tambah_peserta'
		);
		$this->load->view('template/header', $title);
		$this->load->view('content/tambah_peserta');
		$this->load->view('template/footer');
	}

	public function SimpanDataPeserta()
	{
		// $code = $this->input->post('kode_peserta');
		$kode_peserta = $this->input->post('kode_peserta');
		$nik = $this->input->post('nik');
		$no_urut = $this->input->post('no_urut');
		$nama_peserta = $this->input->post('nama_peserta');
		$department = $this->input->post('department');
		$nama_perusahaan = $this->input->post('nama_perusahaan');

		$this->load->library('ciqrcode');

		$config['cacheable']    = true; //boolean, the default is true
		$config['cachedir']     = './assets/'; //string, the default is application/cache/
		$config['errorlog']     = './assets/'; //string, the default is application/logs/
		$config['imagedir']     = './qrcode/'; //direktori penyimpanan qr code
		$config['quality']      = true; //boolean, the default is true
		$config['size']         = '1024'; //interger, the default is 1024
		// $config['max_width']    = '83.149';
		//    $config['max_height']   = '151';
		$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
		$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($config);

		$qr_code = $kode_peserta . '.png'; //buat name dari qr code sesuai dengan nim

		$params['data'] = $kode_peserta; //data yang akan di jadikan QR CODE
		$params['level'] = 'H'; //H=High
		$params['size'] = 10;
		$params['savename'] = FCPATH . $config['imagedir'] . $qr_code; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE


		$this->Model_peserta->simpan_qrcode($kode_peserta, $qr_code); //simpan ke database


		$this->Model_peserta->simpan_peserta($kode_peserta, $nik, $no_urut, $nama_peserta, $department, $nama_perusahaan);//simpan ke database


		// redirect('home/tambah_peserta', 'refresh');
	}

	public function SimpanDataQRcode()
	{
		$code = $this->input->post('kode_peserta');
		$kode_peserta = str_replace(" ", "", $code);

		$this->load->library('ciqrcode');

		$config['cacheable']    = true; //boolean, the default is true
		$config['cachedir']     = './assets/'; //string, the default is application/cache/
		$config['errorlog']     = './assets/'; //string, the default is application/logs/
		$config['imagedir']     = './qrcode/'; //direktori penyimpanan qr code
		$config['quality']      = true; //boolean, the default is true
		$config['size']         = '1024'; //interger, the default is 1024
		// $config['max_width']    = '83.149';
		//    $config['max_height']   = '151';
		$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
		$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($config);

		$qr_code = $kode_peserta . '.png'; //buat name dari qr code sesuai dengan nim

		$params['data'] = $kode_peserta; //data yang akan di jadikan QR CODE
		$params['level'] = 'H'; //H=High
		$params['size'] = 10;
		$params['savename'] = FCPATH . $config['imagedir'] . $qr_code; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE


		$this->Model_peserta->simpan_qrcode($kode_peserta, $qr_code); //simpan ke database


		// redirect('home/tambah_peserta', 'refresh');
	}

	public function edit_data_peserta($id)
	{

		$row = $this->Model_peserta->get_by_id($id);
		$title['title'] = ('Edit Data Peserta');
		$data = array(
			'page' => 'content/edit_data_peserta',
			'kode_peserta' => $row->kode_peserta,
			'nik'	=> $row->nik,
			'nama_peserta' => $row->nama_peserta,
			'department' => $row->department,
			'nama_perusahaan' => $row->nama_perusahaan
		);
		$this->load->view('template/header', $title);
		$this->load->view('template/content', $data);
		$this->load->view('template/footer');
	}

	public function UpdateDataPeserta($id)
	{
		$row = $this->Model_peserta->get_by_id($id);
		$kode_peserta = $this->input->post('kode_peserta');
		$nik = $this->input->post('nik');
		$nama_peserta = $this->input->post('nama_peserta');
		$department = $this->input->post('department');
		$nama_perusahaan = $this->input->post('nama_perusahaan');

		$this->Model_peserta->update_peserta($kode_peserta, $nik, $nama_peserta, $department, $nama_perusahaan);

		// redirect(site_url('home'));
	}

	public function hapus_data_peserta($id)
	{
		$this->Model_peserta->hapus_data_peserta($id);
		// redirect('home', 'refresh');
	}

	public function hapus_data_pemenang($id)
	{
		$this->Model_peserta->hapus_data_pemenang($id);
		// redirect('home', 'refresh');
	}

	public function hapusSemuaDataPeserta()
	{
		$this->Model_peserta->hapus_semua_data_peserta();
		// redirect('home', 'refresh');
	}

	public function daftarHadir()
	{
		$title['title'] = ('Daftar Hadir Peserta');
		$kode_peserta = $this->input->post('qrcode');
		$data = array(
			'page' => 'content/hadir'
		);
		$this->load->view('template/header', $title);
		$this->load->view('template/content', $data);
		$this->load->view('template/footer');
	}

	public function registrasi_peserta()
	{

		// $row = $this->Model_peserta->get_by_id($id);
		$title['title'] = ('Registrasi Peserta');
		$data = array(
			'page' => 'content/registrasi_peserta',
			'hasil' => $this->Model_peserta->dataRegistrasi()
		);
		$this->load->view('template/header', $title);
		$this->load->view('template/content', $data);
		$this->load->view('template/footer');
	}

	public function data_registrasi($id)
	{
		$kode_peserta = $id;
		$row = $this->Model_peserta->get_by_id($kode_peserta);
		if ($row != null) {
			$nik = $row->nik;
			$nama_peserta = $row->nama_peserta;
			$department = $row->department;
			$nama_perusahaan = $row->nama_perusahaan;
		} else {
			$nik = '';
			$nama_peserta = '';
			$department = '';
			$nama_perusahaan = '';
		}
		$title['title'] = ('Data Peserta');
		$data = array(
			'page' => 'content/hadir',
			'hasil' => $this->Model_peserta->dataRegistrasi(),
			'nik'	=> $nik,
			'nama_peserta' => $nama_peserta,
			'department' => $department,
			'nama_perusahaan' => $nama_perusahaan
		);
		$this->load->view('template/header', $title);
		$this->load->view('template/content', $data);
		$this->load->view('template/footer');
	}

	public function undian()
	{
		$title['title'] = ('Undian Peserta');
		$data = array(
			'page' => 'content/undian',
			'data' => $this->Model_peserta->getDataHadiah(),
			'pemenang' => $this->Model_peserta->dataPemenang(),
			'batas' => 0,
			'id_hadiah' => 0
		);
		$this->load->view('template/content', $data);
	}

	public function getUndian($id_hadiah)
	{
		$title['title'] = ('Undian Peserta');
		$batas = $this->Model_peserta->batasJumlah($id_hadiah);
		$data = array(
			'page' => 'content/undian',
			'data' => $this->Model_peserta->getDataHadiah(),
			'pemenang' => $this->Model_peserta->dataPemenang(),
			'id_hadiah' => $id_hadiah,
			'batas'	=> $batas->jumlah
		);
		$this->load->view('template/content', $data);
	}

	public function getDataUndian()
	{
		$id_hadiah = $this->input->get('id_hadiah');
		// $kategori = $this->Model_peserta->getHadiah($id_hadiah);

		// bemasalah disini
		// if (strtolower($kategori->kategori) == "gold") {
		// 	$data['hasil'] = $this->Model_peserta->undianGold(strtolower($kategori->kategori));
		// } elseif (strtolower($kategori->kategori) == "silver") {
		// 	$data['hasil'] = $this->Model_peserta->undianSilver();
		// } else {
		// 	$data['hasil'] = $this->Model_peserta->undianBronze();
		// }
		$data['hasil'] = $this->Model_peserta->undian();
		echo json_encode($data);
	}

	//hadiah
	public function getDataHadiah()
	{
		$data['hasil'] = $this->Model_peserta->getAllDataHadiah();
		echo json_encode($data);
	}

	public function getHadiah()
	{
		$id_hadiah = $this->input->get('id_hadiah');
		$data = $this->Model_peserta->getHadiah($id_hadiah);
		echo json_encode($data);
	}

	public function getBatasJumlah($id_hadiah)
	{
		$data = $this->Model_peserta->batasJumlah($id_hadiah);
		echo json_encode($data);
	}

	public function data_hadiah()
	{
		$title['title'] = ('Data Hadiah');
		$data = array(
			'page' => 'content/data_hadiah'
		);
		$this->load->view('template/header', $title);
		$this->load->view('template/content', $data);
		$this->load->view('template/footer');
	}

	public function insertHadiah()
	{
		$nama_hadiah = $this->input->post('nama_hadiah');
		$jenis_hadiah = $this->input->post('jenis_hadiah');
		// $kategori = $this->input->post('kategori');
		$jumlah = $this->input->post('jumlah');

		$this->Model_peserta->insertHadiah($nama_hadiah, $jenis_hadiah, $jumlah);
	}

	public function updateHadiah()
	{
		$id_hadiah = $this->input->post('id_hadiah');
		$nama_hadiah = $this->input->post('nama_hadiah');
		$jenis_hadiah = $this->input->post('jenis_hadiah');
		// $kategori = $this->input->post('kategori');
		$jumlah = $this->input->post('jumlah');

		$this->Model_peserta->updateHadiah($id_hadiah, $nama_hadiah, $jenis_hadiah, $jumlah);
	}

	public function deleteHadiah($id_hadiah)
	{
		$this->Model_peserta->deleteHadiah($id_hadiah);
	}

	public function qrcode()
	{
		$title['title'] = ('Data QRCode');
		$data = array(
			'page' => 'content/data_qrcode',
			'department' => $this->Model_peserta->department(),
			'hasil' => $this->Model_peserta->dataQRCode()
		);
		// $this->load->view('template/header', $title);
		$this->load->view('template/content', $data);
		// $this->load->view('template/footer');
	}

	public function data_pemenang()
	{
		$title['title'] = ('Data Pemenang');
		$data = array(
			'page' => 'content/data_pemenang',
			'hasil' => $this->Model_peserta->dataPemenang()
		);
		$this->load->view('template/header', $title);
		$this->load->view('template/content', $data);
		$this->load->view('template/footer');
	}
}
